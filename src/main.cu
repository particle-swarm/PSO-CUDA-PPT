#include <iostream>
#include <iomanip>
#include <math.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>

#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h> 
#include <curand_kernel.h>

#define CUDART_INF_F __int_as_float(0x7f800000)

using namespace std;
#define PI_F 3.141592654f 

typedef vector< long double > ldArray1D;
typedef vector< vector< long double > > ldArray2D;
typedef vector< vector< vector <long double > > > ldArray3D;


double sigMoid(double v){
    return 1/(1+exp(-v));
}

long double F1(ldArray2D& R, int Nd, int p) { // Sphere (-100, 100)
    long double Z=0,  Xi;

    for(int i=0; i<Nd; i++){
        Xi = R[p][i];
        Z += Xi*Xi;
    }
    return -Z;

}

long double F2(ldArray2D& R, int Nd, int p) {   // Rastrigin (-10, 10)
    long double Z=0,  Xi;

    for(int i=0; i<Nd; i++){
        Xi = R[p][i];
        Z += (pow(Xi,2) - 10 * cos(2*PI_F*Xi) + 10);
    }
    return -Z;
}
long double F3(ldArray2D& R, int Nd, int p) { // Griewank (-600, 600)
    long double Z, Sum, Prod, Xi;

    Z = 0; Sum = 0; Prod = 1;
    
    for(int i=0; i<Nd; i++){
        Xi = R[p][i];
        Sum  += Xi*Xi;
        Prod *= cos(Xi/sqrt((double)i)+1)/4000.0f; 
        
        if(isnan(Prod)) Prod = 1;
    }
    
    Z = Sum - Prod;
    
    return -Z;
}
long double F4(ldArray2D& R, int Nd, int p) { // Rosenbrock (-10, 10)
    long double Z=0, Xi, XiPlus1;

    for(int i=0; i<Nd-1; i++){
        Xi = R[p][i];
        XiPlus1 = R[p][i+1];
        Z = Z + (100*(XiPlus1-Xi*Xi)*(XiPlus1-Xi*Xi) + (Xi-1)*(Xi-1));
    }
    return -Z;
}

__global__ void setup_kernel(curandState *state){
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    curand_init(clock(), id, 0, &state[id]);
}

__global__ void initPop(float* R, float* V, float* pBestPosition, int Np, int Nd, int xMin, int xMax, int vMin, int vMax, curandState *state){
    int p = threadIdx.x + blockIdx.x * blockDim.x;

    for(int i=0; i<Nd; i++){        
        R[p*Nd+i] = xMin + curand_uniform(&state[p])*(xMax-xMin);
        V[p*Nd+i] = vMin + curand_uniform(&state[p])*(vMax-vMin);
        if(curand_uniform(&state[p]) < 0.5){
            R[p*Nd+i] = -R[p*Nd+i];
            V[p*Nd+i] = -V[p*Nd+i];
        }
        pBestPosition[p*Nd+i] = 0;
    }
}
__global__ void init_Np(float* M, float* pBestValue, float* gBestValue, int Np){
    int p = threadIdx.x + blockIdx.x * blockDim.x;
    if(p < Np){    
        M[p]          = -CUDART_INF_F;
        pBestValue[p] = -CUDART_INF_F;
        *gBestValue   = -CUDART_INF_F;
    }
}

__global__ void init_Nd(float* gBestPosition){
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    gBestPosition[i] = 0;
}

__global__ void evalFitness0(float* R, float* M, int Np, int Nd){
    float Mp;
    int   p  = threadIdx.x + blockIdx.x * blockDim.x;
    
    if(p < Np){   
        Mp = 0;
        for(int i=0; i<Nd; i++){
            Mp += (R[p*Nd+i]*R[p*Nd+i]);
        }   
        M[p] = -Mp;
    }
}

__global__ void updateLocalBest(float* R, float* M, float* pBestValue, float* pBestPosition, int Np, int Nd){
    int p  = threadIdx.x + blockIdx.x * blockDim.x;
    
    if(p < Np){    
        if(M[p] > pBestValue[p]){
            pBestValue[p] = M[p];
            for(int i=0; i<Nd; i++){
                pBestPosition[p*Nd+i] = R[p*Nd+i];
            }
        }
    }
}
// Really should be a reduction
__global__ void updateGlobalBest(float* R, float* M, float* gBestValue, float* gBestPosition, int Np, int Nd){
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    if(id == 0){
        for(int p=0; p<Np; p++){
            if(M[p] > *gBestValue){
                *gBestValue = M[p];
                for(int i=0; i<Nd; i++){
                    gBestPosition[i] = R[p*Nd+i];
                }
            }
        }
    }   
}
__global__ void updateR(float* R, float* V, curandState *state, int Np, int Nd, float xMin, float xMax){
    int p  = threadIdx.x + blockIdx.x * blockDim.x;
    curandState localState = state[p];
    float pos;
     
    if(p < Np){    
        for(int i=0; i<Nd; i++){
            pos = R[p*Nd+i];
            pos += V[p*Nd+i];
            if(pos > xMax) pos = xMin + curand_uniform(&localState)*(xMax-xMin);
            if(pos < xMin) pos = xMin + curand_uniform(&localState)*(xMax-xMin);
            R[p*Nd+i] = pos;    
        }
    }
}
__global__ void updateV(float* V, float* R, float* pBestPosition, float* gBestPosition, curandState *state, float chi, int Np, int Nd, int vMin, int vMax, int C1, int C2){
    int p  = threadIdx.x + blockIdx.x * blockDim.x;
    float Vel;
    curandState localState = state[p];    
    
    if(p < Np){    
        for(int i=0; i<Nd; i++){
            Vel = V[(p*Nd+i)];
            Vel = chi * Vel + C1*curand_uniform(&localState)*(pBestPosition[p*Nd+i] - R[p*Nd+i]) + C2*curand_uniform(&localState)*(gBestPosition[i] - R[p*Nd+i]);   
            if(Vel > vMax) Vel = vMin + curand_uniform(&localState)*(vMax-vMin);
            if(Vel < vMin) Vel = vMin + curand_uniform(&localState)*(vMax-vMin);  
            V[p*Nd+i] = Vel; 
        }
        state[p] = localState;
    }
}

void PSO(int Np, int Nd, int Nt, long double xMin, long double xMax, long double vMin, long double vMax,long double (*objFunc)(ldArray2D& ,int, int), int& numEvals, string functionName){
    // cudaPrintfInit();
    
    float gBestValue;
    float C1, C2;
    float w, wMax, wMin;
    float threadsPerBlock = 32, numBlocks;
    int lastStep;
    
    // CStopWatch timer, timer1;
    StopWatchInterface *timer, *timer1;
    sdkCreateTimer(&timer);
    sdkCreateTimer(&timer1);

    long double positionTime = 0, fitnessTime = 0, velocityTime = 0, totalTime = 0, initTime = 0;
        
    float *R_GPU, *V_GPU, *M_GPU,
          *pBestPosition_GPU, *gBestPosition_GPU, 
          *pBestValue_GPU, *gBestValue_GPU;
    
    gBestValue = -INFINITY;
    C1 = 2.05; C2 = 2.05;
    wMin = 0.4; wMax = 0.9;
    lastStep = Nt;
    numEvals = 0;
    positionTime = 0; fitnessTime = 0; velocityTime = 0; totalTime = 0;

    // Allocate on GPU
    checkCudaErrors(cudaMalloc((void**) &V_GPU,             Np*Nd*sizeof(float)));
    checkCudaErrors(cudaMalloc((void**) &R_GPU,             Np*Nd*sizeof(float)));
    checkCudaErrors(cudaMalloc((void**) &M_GPU,             Np   *sizeof(float)));
    checkCudaErrors(cudaMalloc((void**) &pBestPosition_GPU, Np*Nd*sizeof(float)));
    checkCudaErrors(cudaMalloc((void**) &pBestValue_GPU,    Np*   sizeof(float)));
    checkCudaErrors(cudaMalloc((void**) &gBestPosition_GPU, Nd*   sizeof(float)));
    checkCudaErrors(cudaMalloc(         &gBestValue_GPU,          sizeof(float)));

    // timer1.startTimer();
    sdkStartTimer(&timer1);

    // Set up RNG
    curandState *devStates;
    checkCudaErrors(cudaMalloc((void **)&devStates, Np*Nd*sizeof(curandState)));

    if(Np <= threadsPerBlock){
        numBlocks = 1;
        threadsPerBlock = Np;
    }else{
        numBlocks = ceil(Np/threadsPerBlock);
    }

    setup_kernel<<<numBlocks, threadsPerBlock>>>(devStates); 
    getLastCudaError("setup_kernel execution failed");

    initPop<<<numBlocks, threadsPerBlock>>>(R_GPU, V_GPU, pBestPosition_GPU, Np, Nd, xMin, xMax, vMin, vMax, devStates); 
    getLastCudaError("initPop execution failed");

    init_Np<<<Np, 1>>>(M_GPU, pBestValue_GPU, gBestValue_GPU, Np); 
    getLastCudaError("init_Np execution failed");

    init_Nd<<<Nd, 1>>>(gBestPosition_GPU); 
    getLastCudaError("init_Nd execution failed");
    
    // timer1.stopTimer();
    // initTime += timer1.getElapsedTime();
    sdkStopTimer(&timer1);
    initTime += sdkGetTimerValue(&timer1);
    evalFitness0<<<numBlocks, threadsPerBlock>>>(R_GPU, M_GPU, Np, Nd); 
    getLastCudaError("evalFitness0 execution failed");

    numEvals += Np; 

    for(int j=1; j<Nt; j++){
    
        sdkStartTimer(&timer);
        updateR<<<numBlocks, threadsPerBlock>>>(R_GPU, V_GPU, devStates,Np,  Nd, xMin, xMax);
        getLastCudaError("updateR execution failed");
        sdkStopTimer(&timer);
        positionTime += sdkGetTimerValue(&timer);

        // Evaluate Fitness
        sdkStartTimer(&timer);
        evalFitness0<<<numBlocks, threadsPerBlock>>>(R_GPU, M_GPU, Np, Nd); 
        getLastCudaError("evalFitness0 execution failed");

        updateGlobalBest<<<1,1>>>(R_GPU, M_GPU, gBestValue_GPU, gBestPosition_GPU, Np, Nd); 
        getLastCudaError("updateGlobalBest execution failed");

        updateLocalBest<<<numBlocks,threadsPerBlock>>>(R_GPU, M_GPU, pBestValue_GPU, pBestPosition_GPU, Np, Nd); getLastCudaError("updateLocalBest execution failed");
        
        checkCudaErrors(cudaMemcpy(&gBestValue, gBestValue_GPU, sizeof(float), cudaMemcpyDeviceToHost));
        numEvals += Np;
        sdkStopTimer(&timer);
        fitnessTime += sdkGetTimerValue(&timer);
        
        if(gBestValue >= -0.0001){
            lastStep = j;
            break;
        }

        // Update Velocities
        sdkStartTimer(&timer);
        w = wMax - ((wMax-wMin)/Nt) * j;
        updateV<<<numBlocks, threadsPerBlock>>>(V_GPU, R_GPU, pBestPosition_GPU, gBestPosition_GPU, devStates, w, Np, Nd, vMin, vMax, C1, C2); 
        getLastCudaError("updateR execution failed");
        sdkStopTimer(&timer);
        velocityTime += sdkGetTimerValue(&timer);
    } // End Time Steps

    sdkStopTimer(&timer1);
    totalTime += sdkGetTimerValue(&timer1);

    sdkDsdkDeleteTimer(&timer);
    sdkDeleteTimer(&timer1);
    
    // cudaPrintfEnd();
    checkCudaErrors(cudaFree(pBestPosition_GPU));
    checkCudaErrors(cudaFree(pBestValue_GPU));
    checkCudaErrors(cudaFree(gBestPosition_GPU));
    checkCudaErrors(cudaFree(gBestValue_GPU));
    checkCudaErrors(cudaFree(R_GPU));
    checkCudaErrors(cudaFree(V_GPU));
    checkCudaErrors(cudaFree(M_GPU));
    checkCudaErrors(cudaFree(devStates));
        
    cout    << functionName << " "
            << gBestValue   << " " 
            << Np           << " "
            << Nd           << " "
            << lastStep     << " "
            << numEvals     << " "
            << positionTime << " "
            << fitnessTime  << " "
            << velocityTime << " "
            << totalTime    << endl;
}

void run_PSO(long double xMin, long double xMax, long double vMin, long double vMax, long double (*rPtr)(ldArray2D& , int, int), string functionName){

    int Np, Nd, Nt, numEvals;
    int NdMin, NdMax, NdStep;
    int NpMin, NpMax, NpStep;
    cudaDeviceProp deviceProp;
    int deviceCount, dev;
    
    Nt = 1000;
    NdMin = 32; NdMax = 4096; NdStep = 2;
    NpMin = 32; NpMax = 4096; NpStep = 2;
    cudaSetDevice(0);   
    
    for(Np=NpMin; Np <= NpMax; Np *= NpStep){
        for(Nd=NdMin; Nd <= NdMax; Nd *= NdStep){
            for(int x=0; x<1; x++){
                cudaGetDeviceCount(&deviceCount);
                cudaGetDeviceProperties(&deviceProp, dev);
                PSO(Np, Nd, Nt, xMin, xMax, vMin, vMax, rPtr, numEvals, functionName);
            }
        }
    }

//    int Nt, deviceCount, dev, numEvals;
//    cudaDeviceProp deviceProp;
//    vector<int> Np (3, 0);vector<int> Nd (3, 0);
//    
//    Np[0] = 100; Np[1]=500; Np[2]=1000;
//    Nd[0] = 100; Nd[1]=500; Nd[2]=1000;
//    Nt = 10000;
//    
//    cudaSetDevice(1);  
//    for(int i=0; i< Np.size(); i++){
//        for(int j=0; j< Nd.size(); j++){
//            for(int x=0; x<10; x++){
//                cudaGetDeviceCount(&deviceCount);
//                cudaGetDeviceProperties(&deviceProp, dev);
//                PSO(Np[i], Nd[j], Nt, xMin, xMax, vMin, vMax, rPtr, numEvals, functionName);
//            }
//        }
//    }
    
}
int main(){

    int deviceCount=0, dev=0;
    long double (*rPtr)(ldArray2D& , int, int) = NULL;
    long double xMin, xMax, vMin, vMax;
    cudaDeviceProp deviceProp;
    
    cudaGetDeviceCount(&deviceCount);
    cudaGetDeviceProperties(&deviceProp, dev);
    
    cout << "Function, Fitness, Np, Nd, Last Step, Evals, Position Time, Fitness Time, Velocity Time, Total Time" << endl;
    
    rPtr = &F1; //Np = 32;
    xMin = -100; xMax = 100;
    vMin = -100; vMax = 100;
    run_PSO(xMin, xMax, vMin, vMax, rPtr, "F1");


    rPtr = NULL;

    return 0;
}
